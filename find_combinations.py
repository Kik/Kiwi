#!/usr/bin/python
# autor: Kristina Miklasova, tina.miklasova@gmail.com
#
# Princip mojho algoritmu:
# Nacitava lety z stdin a uklada ich do listu listov (fcia save_flight_into_list) - vsetky lety, kt. maju
# rovnake source a destination su v jednom liste (odchody a prichody su typu datetime) - napr.:
# [ 'USM', 'HKT', [datetime.datetime(2016, 10, 11, 10, 10), datetime.datetime(2016, 10, 11, 11, 10), 'PV511'],
#  [datetime.datetime(2016, 10, 11, 10, 30), datetime.datetime(2016, 10, 11, 11, 30), 'PV913'] ]
#
# Po nacitani vstupu zoradi jednotlive listy podla datumu a casu odchodu a prichodu.
# Najde vsetky vyhovujuce dvojice letov a podla nich hlada dlhsie kombinacie letov.
#
# Hlavny while cyklus (hlada vsetky kombinacie letov - vychadza z dvojic letov):
#   Prekopiruje aktualny list kombinacii letov do pomocneho listu, zavola fciu na vyhladanie kombinacii letov
#   ('find_all_combinations') a odstrani nevalidne kombinacie (A->B->A->B, kde A,B su nazvy letisk) a duplikaty
#   (z A->B; A->B->C odstrani A->B). Cyklus zastavi, ked uz nepribudne ziadna nova kombinacia, ani nie je odstranena
#   uz existujuca kombinacia.
#
# Na vystup vypise kombinacie len s kodmi letov (kombinacie su ulozene v liste v tvare: PV534 -> PV612;DPS,BWN,DPS
# cize v pripade potreby je mozne dalej pracovat aj s nazvami letisk).

import sys
import operator
import datetime


# vrati datum typu datetime, kt. vznikne z 'date_string' tvaru 2016-10-11T10:10:00
def extract_date(date_string):
    date = datetime.datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S")
    return date


# rozdeli 'other_info' tvaru [2016-10-11T10:10:00,2016-10-11T11:10:00,PV511] na [odlet,prilet,cislo_letu]
# odlet a prilet su typu datetime
def split_other_info(other_info):
    departure = extract_date(other_info[0])
    arrival = extract_date(other_info[1])
    flight_number = other_info[2][:len(other_info[2])-1]     # odstranenie znaku noveho riadku "\n"
    return [departure,arrival,flight_number]


# usporiada list vsetkych letov podla datumu a casu priletu a odletu
def sort_flights(flight):
    flight[2:] = sorted(flight[2:], key = operator.itemgetter(0,1))
    return flight


# zisti, ci sa kombinacia "source,destination" nachadza v zozname
# ak ano, tak prida do listu tejto kombinacie udaje o lete (other_info)
# inak prida novu kombinaciu az na koniec list_of_flights
def save_flight_into_list(list_of_flights, source, destination, other_info):
    other_info = split_other_info(other_info)
    for flight in list_of_flights:
        if flight[0] == source and flight[1] == destination:
            flight.append(other_info)
            return list_of_flights
    list_of_flights.append([source,destination,other_info])
    return list_of_flights


# najde vsetky vyhovujuce dvojice letov a ulozi ich do listu 'flight_pairs'
# ak sa rovna destination 1. letu so source 2. letu a zaroven je zachovany casovy rozdiel na prestup 1-4 hod.,
# spoji kody letov a nazvy letisk a ulozi ich do listu vyhovujucich dvojic
def find_flight_pairs(list_of_flights):
    flight_pairs = []
    for flight1 in list_of_flights:
        for flight2 in list_of_flights:
            if flight1[1] == flight2[0]:
                for i in range(2, len(flight1)):
                    for j in range(2, len(flight2)):
                        # ak je rozdiel viac ako 4 hod., dalsie lety sa uz nemusia skusat, lebo su usporiadane podla casu
                        if (flight2[j][0]-flight1[i][1]).total_seconds() > 14400:
                            break
                        # rozdiel priletu - odletu je medzi 1-4 hod, prida do zoznamu kody letov a nazvy letisk
                        elif 3600 <= (flight2[j][0]-flight1[i][1]).total_seconds() <= 14400:
                            flight_pairs.append(flight1[i][2] + " -> " + flight2[j][2] + ";" + flight1[0] + "," + flight1[1] + "," + flight2[1])
    return flight_pairs


# spaja dosial najdene kombinacie (flight_combinations) s vhodnou dvojicou moznych letov (flight_pairs)
# ak ex. v flight_pairs taka dvojica, ze sa zacina letom, na kt. sa flight_combination konci, tak ich spoji
# flight_pair: A -> B; flight_combination: C -> D -> A  ===>  C -> D -> A -> B
# za kazde spojene lety prida aj nazvy letisk
def find_all_combinations(flight_pairs, flight_combinations):
    temp = flight_combinations[:]
    for comb in flight_combinations:
        for pair in flight_pairs:
            comb_parts = comb.split(";")            # comb v tvare: PV511 -> PV840;USM,HKT,USM
            pair_parts = pair.split(";")            # pair v tvare: PV840 -> PV221;HKT,USM,HKT
            comb_flights = comb_parts[0].split(" -> ")       # PV511 -> PV840
            pair_flights = pair_parts[0].split(" -> ")       # PV840 -> PV221
            pair_places = pair_parts[1].split(",")           # HKT,USM,HKT
            if comb_flights[len(comb_flights)-1] == pair_flights[0]:    # PV840 == PV840
                # vysledna kombinacia: PV511 -> PV840 -> PV221;USM,HKT,USM,HKT
                temp.append(comb_parts[0] + " -> " + pair_flights[1] + ";" + comb_parts[1] + "," + pair_places[2])
    return temp


# fcia 'find_all_combinations' pridava nove kombinacie na koniec, t.j. podretazce letov zostavaju v zozname
# preto zmaze vsetky lety, kt. sa vyskytuju na zaciatku inych (dlhsich) letov
# 'flight' aj lety vo 'flight_combinations' su v tvare: PV511 -> PV840 -> PV221;USM,HKT,USM,HKT
#  napr. lety: A -> B; A -> B -> C; L -> M; K -> L -> M
# ===> zmaze len A -> B, ale nie L -> M, pretoze sa L -> M  v K -> L -> M nevyskytuje ako podretazec na zaciatku
def remove_duplicate_combinations(flight_combinations, flight):
    flight_parts = flight.split(";")
    for combination in flight_combinations:
        comb_parts = combination.split(";")
        if combination == flight:
            continue
        elif comb_parts[0].startswith(flight_parts[0]):
            return True
    return False


# zmaze kombinaciu letov, kt. v sebe obsahuje kombinaciu A -> B -> A -> B, kde A,B su nazvy letisk
# overuje len lety, kt. letia aspon cez 4 miesta (t.j. A -> B -> C nekontroluje)
def remove_invalid_combinations(flight_comb):
    flight_parts = flight_comb.split(";")
    flight_places = flight_parts[1].split(",")
    if len(flight_places) > 3:
        for i in range(0, len(flight_places)-3):
            if flight_places[i] == flight_places[i+2] and flight_places[i+1] == flight_places[i+3]:
                return True
        return False
    return False


# # # # # # # # # # MAIN PART # # # # # # # # # #
list_of_flights = []
list_of_flight_pairs = []
all_flight_combinations = []

for line in sys.stdin:
    if not line.startswith("source"):
        line_parts = line.split(",",4)
        # save_flight_into_list(list_of_flights, source, destination, [departure, arrival, flight_number])
        list_of_flights = save_flight_into_list(list_of_flights,line_parts[0],line_parts[1],line_parts[2:5])

for i in range(len(list_of_flights)):
    list_of_flights[i] = sort_flights(list_of_flights[i])

list_of_flight_pairs = find_flight_pairs(list_of_flights)
all_flight_combinations = find_all_combinations(list_of_flight_pairs, list_of_flight_pairs)

temp = []
while not len(set(all_flight_combinations).intersection(temp)) > 0:   # ekvivalentne s 'while all_flight_combinations != temp'
    temp = all_flight_combinations[:]
    all_flight_combinations = find_all_combinations(list_of_flight_pairs, all_flight_combinations)
    all_flight_combinations = [flight for flight in all_flight_combinations if not remove_invalid_combinations(flight)]
    all_flight_combinations = [flight for flight in all_flight_combinations if not remove_duplicate_combinations(all_flight_combinations, flight)]

# vypis kombinacii letov na stdout
# kombinacie v liste 'all_flight_combinations' su v tvare: PV534 -> PV612;DPS,BWN,DPS (na stdout vypise len kody letov)
for combination in all_flight_combinations:
    comb_parts = combination.split(";")
    print(comb_parts[0])



